package wallOfTweets;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.text.DateFormat;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Vector;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class WoTServlet
 */
@WebServlet("/")
public class WoTServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Locale currentLocale = new Locale("en");
	String ENCODING = "ISO-8859-1";

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public WoTServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String header = request.getHeader("Accept");
		try {
			Vector<Tweet> tweets = Database.getTweets();
			if(header.equals("text/plain")) {
				printPlainTextresult(tweets, request, response);
			}else {
				printHTMLresult(tweets, request, response);	
			}
			
		}

		catch (SQLException ex ) {
			throw new ServletException(ex);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String action = request.getParameter("action");
		if (action.equals("Tweet!")) {
	        String author = request.getParameter("author");
	        String tweet_text = request.getParameter("tweet_text");
	        String header = request.getHeader("Accept");
	        try {
	            long tweetID = Database.insertTweet(author, tweet_text);
	            if (tweetID != -1) {
		            // hash the tweet ID with md5
		            String tweetIDString = Long.toString(tweetID);
		            String tweetIDHash = hashTweetID(tweetIDString);
		            Cookie tweetIDCookie = new Cookie("tweet" + tweetIDString, tweetIDHash);
		            response.addCookie(tweetIDCookie);
	            } // else not created: drop silently
	            if (header.equals("text/plain")){
	            	PrintWriter writer = response.getWriter();
	            	writer.println(tweetID);
//	            	writer.println("Tweet hash:" + tweetIDHash);
	            } else { // text/html
	            	response.sendRedirect(request.getContextPath());
	            }
	        } catch (SQLException e) {
	            e.printStackTrace();
	        } catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
			}
		} else if (action.equals("Delete")) {
			String tweet_id_string = request.getParameter("tweet_id");
			if (tweet_id_string.matches("[0-9]*")) {
				long tweetID = Long.parseLong(tweet_id_string);
				// check for cookie
	            String tweetIDString = Long.toString(tweetID);
	            String tweetIDHash;
				try {
					tweetIDHash = hashTweetID(tweetIDString);
					boolean hasCookie = false;
					for (Cookie tweetHash : request.getCookies()) {
						if (tweetHash.getValue().equals(tweetIDHash)) {
							hasCookie = true;
							tweetHash.setMaxAge(0); // set cookie to expire now
							response.addCookie(tweetHash);
							break;
						}
					}
					boolean deleteSuccessful = false;
					if (hasCookie) {					
						deleteSuccessful = Database.deleteTweet(tweetID);
					}
					String header = request.getHeader("Accept");
					if (header.equals("text/plain")){
						PrintWriter writer = response.getWriter();
						writer.println(deleteSuccessful);
					} else { // text/html
						response.sendRedirect(request.getContextPath());
					}
				} catch (NoSuchAlgorithmException e) {
					e.printStackTrace();
				} // else incorrect cookie: drop silently
			} // else incorrect tweet_id: drop silently
		} // else incorrect action: drop silently
	}

	private String hashTweetID(String tweetIDString) throws NoSuchAlgorithmException {
		MessageDigest md = MessageDigest.getInstance("MD5");
		String salt = "ASW";
		String toBeHashed = tweetIDString + salt;
		md.update(toBeHashed.getBytes());
		byte[] digest = md.digest();
		String tweetIDHash = String.format("%032x", new BigInteger(1, digest));
		return tweetIDHash;
	}

	private void printHTMLresult (Vector<Tweet> tweets, HttpServletRequest req, HttpServletResponse res) throws IOException
	{
		DateFormat dateFormatter = DateFormat.getDateInstance(DateFormat.FULL, currentLocale);
		DateFormat timeFormatter = DateFormat.getTimeInstance(DateFormat.DEFAULT, currentLocale);
		String currentDate = dateFormatter.format(new java.util.Date());
		res.setContentType ("text/html");
		res.setCharacterEncoding(ENCODING);
		PrintWriter  out = res.getWriter ( );
		out.println("<!DOCTYPE html>");
		out.println("<html>");
		out.println("<head><title>Wall of Tweets</title>");
		out.println("<link href=\"wallstyle.css\" rel=\"stylesheet\" type=\"text/css\" />");
		out.println("</head>");
		out.println("<body class=\"wallbody\">");
		out.println("<h1>Wall of Tweets</h1>");
		out.println("<div class=\"walltweet\">"); 
		out.println("<form method=\"post\">");
//		out.println("<input type=\"hidden\" name=\"action\" value=\"tweet\">");
		out.println("<table border=0 cellpadding=2>");
		out.println("<tr><td>Your name:</td><td><input name=\"author\" type=\"text\" size=70></td><td></td></tr>");
		out.println("<tr><td>Your tweet:</td><td><textarea name=\"tweet_text\" rows=\"2\" cols=\"70\" wrap></textarea></td>"); 
		out.println("<td><input type=\"submit\" name=\"action\" value=\"Tweet!\"></td></tr>"); 
		out.println("</table></form></div>"); 
		for (Tweet tweet: tweets) {
			String messDate = dateFormatter.format(tweet.getDate());
			if (!currentDate.equals(messDate)) {
				out.println("<br><h3>...... " + messDate + "</h3>");
				currentDate = messDate;
			}
			out.println("<div class=\"wallitem\">");
			out.println("<h4><em>" + tweet.getAuthor() + "</em> @ "+ timeFormatter.format(tweet.getDate()) +"</h4>");
			out.println("<p>" + tweet.getText() + "</p>");
			out.println("<form method=\"post\">");
//			out.println("<input type=\"hidden\" name=\"action\" value=\"delete\">");
			out.println("<input type=\"submit\" name=\"action\" value=\"Delete\">");
			out.println("<input name=\"tweet_id\" type=\"hidden\" value=\"" + tweet.getTwid() + "\">");
			out.print("</form>");
			out.println("</div>");
		}
		out.println ( "</body></html>" );
	}
	private void printPlainTextresult (Vector<Tweet> tweets, HttpServletRequest req, HttpServletResponse res) throws IOException
	{
		DateFormat dateFormatter = DateFormat.getDateInstance(DateFormat.FULL, currentLocale);
		DateFormat timeFormatter = DateFormat.getTimeInstance(DateFormat.DEFAULT, currentLocale);
		String currentDate = dateFormatter.format(new java.util.Date());
		res.setContentType ("text/plain");
		res.setCharacterEncoding(ENCODING);
		PrintWriter out = res.getWriter();
		
		Function<? super Tweet, ? extends String> tweeetMapper = 
				t -> "tweet" + " #" + t.getTwid() + ": " + t.getAuthor() + ": " + t.getText() + " [" + t.getDate().toString() + "]";
		out.println(tweets.stream()
			.map(tweeetMapper)
			.collect(Collectors.joining("\n")));
	}
}
