package asw01cs;


import org.apache.http.client.fluent.Content;
import org.apache.http.client.fluent.Form;
import org.apache.http.client.fluent.Request;
//This code uses the Fluent API

public class SimpleFluentClient {

	private static String URI = "http://localhost:8080/waslab01_ss/";

	public final static void main(String[] args) throws Exception {
    	
    	/* Insert code for Task #4 here */
        Content response = Request.Post(URI)
                .setHeader("Accept", "text/plain")
                .bodyForm(Form.form()
                		.add("action", "Tweet!")
                		.add("author", "Devil")
                		.add("tweet_text", "You will die!")
                		.build())
                .execute()
                .returnContent();
        String tweetID = response.asString().split("\n")[0]; // get only value before \n

    	System.out.println(Request.Get(URI)
    			.setHeader("Accept", "text/plain")
    			.execute().returnContent());
    	
    	/* Insert code for Task #5 here */
    	System.out.println(Request.Post(URI)
    			.setHeader("Accept", "text/plain")
    			.bodyForm(Form.form()
    					.add("action", "Delete")
    					.add("tweet_id", tweetID)
    					.build())
    			.execute()
    			.returnContent());
    }
}

